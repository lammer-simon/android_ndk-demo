package simonlammer.htlleonding.ac.at.androidndk_demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
        CppWorker.registerNativeMethods();
    }

    private List<PerformanceComparator> comparators = new LinkedList<>();
    private Map<PerformanceComparator, PerformanceComparison> comparisons = new HashMap<>();
    private Timer timer = new Timer();
    private ProgressDialog progressDialog;

    @Override
    protected void onStart() {
        super.onStart();
        loadNativeLibrary();
    }

    private native void loadNativeLibrary();

    @Override
    protected void onStop() {
        unloadNativeLibrary();
        super.onStop();
    }

    private native void unloadNativeLibrary();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("This may take a while...");
        progressDialog.setCancelable(false);

        JavaWorker javaWorker = new JavaWorker();
        CppWorker cppWorker = new CppWorker();

        javaWorker.printHelloWorld();
        cppWorker.printHelloWorld();

        String returnPrimitivesResult = String.format("         Java | C++\n" +
                "boolean: %-5s %-5s\n" +
                "byte:    %5d %5d\n" +
                "short:   %5d %5d\n" +
                "int:     %5d %5d\n" +
                "long:    %5d %5d\n" +
                "float:   %5.1f %5.1f\n" +
                "double:  %5.1f %5.1f\n" +
                "char:        %c     %c",
                javaWorker.returnBoolean() ? "true" : "false", cppWorker.returnBoolean() ? "true" : "false",
                javaWorker.returnByte(), cppWorker.returnByte(),
                javaWorker.returnShort(), cppWorker.returnShort(),
                javaWorker.returnInt(), cppWorker.returnInt(),
                javaWorker.returnLong(), cppWorker.returnLong(),
                javaWorker.returnFloat(), cppWorker.returnFloat(),
                javaWorker.returnDouble(), cppWorker.returnDouble(),
                javaWorker.returnChar(), cppWorker.returnChar());
        Log.d("MainActivity", returnPrimitivesResult);

        // Example of a call to a native method
        TextView tv = (TextView) findViewById(R.id.sample_text);
        tv.setText(stringFromJNI() + "\n\n" + returnPrimitivesResult);

        SeekBar seekBar = (SeekBar) findViewById(R.id.seek_bar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                for (PerformanceComparator comparator : comparators) {
                    comparator.setRuns(comparator.getInitialRuns() * (progress + 1) / 100);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // intentionally empty
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // intentionally empty
            }
        });

        initializeComparators(javaWorker, cppWorker);

        seekBar.setProgress(1499);

        TableLayout tableLayout = (TableLayout) findViewById(R.id.table_layout);
        final List<View> performanceComparatorViews = new LinkedList<>();
        for (PerformanceComparator comparator : comparators) {
            View performanceComparatorView = createPerformanceComparisonView(comparator);
            performanceComparatorViews.add(performanceComparatorView);
            tableLayout.addView(performanceComparatorView);
        }
        Button runAllTests = (Button) findViewById(R.id.button_run_all_tests);
        runAllTests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (View view : performanceComparatorViews) {
                    CheckBox checkbox = (CheckBox) view.findViewById(R.id.checkbox);
                    if (checkbox.isChecked()) {
                        Button runTest = (Button) view.findViewById(R.id.button);
                        runTest.performClick();
                    }
                }
            }
        });

        Button sendTestResults = (Button) findViewById(R.id.button_send_comparison_results);
        sendTestResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comparisons.isEmpty()) {
                    Toast.makeText(MainActivity.this, "No results to send. Please run tests.", Toast.LENGTH_SHORT).show();
                } else {
                    StringBuilder mailBody = new StringBuilder();
                    for (PerformanceComparator comparator : comparators) {
                        if (comparisons.containsKey(comparator)) {
                            PerformanceComparison comparison = comparisons.get(comparator);
                            mailBody.append("Test: ");
                            mailBody.append(comparator.getName());
                            mailBody.append("\n");
                            mailBody.append(comparison.toString());
                            mailBody.append("\n\n");
                        }
                    }
                    mailBody.append("Which phone are you using?\n");

                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"lammer.simon+ndktestresults@gmail.com"});
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Test results");
                    intent.putExtra(Intent.EXTRA_TEXT, mailBody.toString());

                    startActivity(Intent.createChooser(intent, "Send Email"));
                }
            }
        });
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    private View createPerformanceComparisonView(final PerformanceComparator comparator) {
        final View view = getLayoutInflater().inflate(R.layout.performance_comparator_view, null);
        final TextView textView_text = (TextView) view.findViewById(R.id.text);
        final Button button = (Button) view.findViewById(R.id.button);
        final CheckBox checkbox = (CheckBox) view.findViewById(R.id.checkbox);
        final TextView testView_comparisonResultJavaRuns       = (TextView) view.findViewById(R.id.comparison_result_java_runs);
        final TextView testView_comparisonResultJavaAvg        = (TextView) view.findViewById(R.id.comparison_result_java_avg);
        final TextView testView_comparisonResultJavaQ1       = (TextView) view.findViewById(R.id.comparison_result_java_q1);
        final TextView testView_comparisonResultJavaMean       = (TextView) view.findViewById(R.id.comparison_result_java_mean);
        final TextView testView_comparisonResultJavaQ3       = (TextView) view.findViewById(R.id.comparison_result_java_q3);
        final TextView testView_comparisonResultJavaMin        = (TextView) view.findViewById(R.id.comparison_result_java_min);
        final TextView testView_comparisonResultJavaMax        = (TextView) view.findViewById(R.id.comparison_result_java_max);
        final TextView testView_comparisonResultJavaSum        = (TextView) view.findViewById(R.id.comparison_result_java_sum);
        final TextView testView_comparisonResultCRuns          = (TextView) view.findViewById(R.id.comparison_result_c_runs);
        final TextView testView_comparisonResultCAvg           = (TextView) view.findViewById(R.id.comparison_result_c_avg);
        final TextView testView_comparisonResultCQ1          = (TextView) view.findViewById(R.id.comparison_result_c_q1);
        final TextView testView_comparisonResultCMean          = (TextView) view.findViewById(R.id.comparison_result_c_mean);
        final TextView testView_comparisonResultCQ3          = (TextView) view.findViewById(R.id.comparison_result_c_q3);
        final TextView testView_comparisonResultCMin           = (TextView) view.findViewById(R.id.comparison_result_c_min);
        final TextView testView_comparisonResultCMax           = (TextView) view.findViewById(R.id.comparison_result_c_max);
        final TextView testView_comparisonResultCSum           = (TextView) view.findViewById(R.id.comparison_result_c_sum);
        final TextView testView_comparisonResultDifferenceRuns = (TextView) view.findViewById(R.id.comparison_result_difference_runs);
        final TextView testView_comparisonResultDifferenceAvg  = (TextView) view.findViewById(R.id.comparison_result_difference_avg);
        final TextView testView_comparisonResultDifferenceQ1 = (TextView) view.findViewById(R.id.comparison_result_difference_q1);
        final TextView testView_comparisonResultDifferenceMean = (TextView) view.findViewById(R.id.comparison_result_difference_mean);
        final TextView testView_comparisonResultDifferenceQ3 = (TextView) view.findViewById(R.id.comparison_result_difference_q3);
        final TextView testView_comparisonResultDifferenceMin  = (TextView) view.findViewById(R.id.comparison_result_difference_min);
        final TextView testView_comparisonResultDifferenceMax  = (TextView) view.findViewById(R.id.comparison_result_difference_max);
        final TextView testView_comparisonResultDifferenceSum  = (TextView) view.findViewById(R.id.comparison_result_difference_sum);
        final TextView testView_comparisonResultRelativeRuns   = (TextView) view.findViewById(R.id.comparison_result_relative_runs);
        final TextView testView_comparisonResultRelativeAvg    = (TextView) view.findViewById(R.id.comparison_result_relative_avg);
        final TextView testView_comparisonResultRelativeQ1   = (TextView) view.findViewById(R.id.comparison_result_relative_q1);
        final TextView testView_comparisonResultRelativeMean   = (TextView) view.findViewById(R.id.comparison_result_relative_mean);
        final TextView testView_comparisonResultRelativeQ3   = (TextView) view.findViewById(R.id.comparison_result_relative_q3);
        final TextView testView_comparisonResultRelativeMin    = (TextView) view.findViewById(R.id.comparison_result_relative_min);
        final TextView testView_comparisonResultRelativeMax    = (TextView) view.findViewById(R.id.comparison_result_relative_max);
        final TextView testView_comparisonResultRelativeSum    = (TextView) view.findViewById(R.id.comparison_result_relative_sum);
        textView_text.setText(comparator.getName());
        checkbox.setChecked(comparator.isSelected());
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                comparator.setSelected(isChecked);
            }
        });
        button.setText("Run Test");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setEnabled(false);
                button.setText("Processing");
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.setTitle("Running test " + comparator.getName());
                                progressDialog.show();
                            }
                        });
                        final PerformanceComparison performanceComparison = comparator.investigateTimeDifference();
                        comparisons.put(comparator, performanceComparison);
                        Log.i("PerformanceComparison-" + comparator.getName(), performanceComparison.toString());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                testView_comparisonResultJavaRuns.setText(Integer.toString(performanceComparison.getRuns()));
                                testView_comparisonResultJavaAvg.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getAverageExecutionTime()));
                                testView_comparisonResultJavaQ1.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getQuartil1ExecutionTime()));
                                testView_comparisonResultJavaMean.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getMeanExecutionTime()));
                                testView_comparisonResultJavaQ3.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getQuartil3ExecutionTime()));
                                testView_comparisonResultJavaMin.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getMinimumExecutionTime()));
                                testView_comparisonResultJavaMax.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getMaximumExecutionTime()));
                                testView_comparisonResultJavaSum.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getTotalExecutionTime()));
                                testView_comparisonResultCRuns.setText(Integer.toString(performanceComparison.getRuns()));
                                testView_comparisonResultCAvg.setText(Long.toString(performanceComparison.getWorkerBAnalyzation().getAverageExecutionTime()));
                                testView_comparisonResultCQ1.setText(Long.toString(performanceComparison.getWorkerBAnalyzation().getQuartil1ExecutionTime()));
                                testView_comparisonResultCMean.setText(Long.toString(performanceComparison.getWorkerBAnalyzation().getMeanExecutionTime()));
                                testView_comparisonResultCQ3.setText(Long.toString(performanceComparison.getWorkerBAnalyzation().getQuartil3ExecutionTime()));
                                testView_comparisonResultCMin.setText(Long.toString(performanceComparison.getWorkerBAnalyzation().getMinimumExecutionTime()));
                                testView_comparisonResultCMax.setText(Long.toString(performanceComparison.getWorkerBAnalyzation().getMaximumExecutionTime()));
                                testView_comparisonResultCSum.setText(Long.toString(performanceComparison.getWorkerBAnalyzation().getTotalExecutionTime()));
                                testView_comparisonResultDifferenceRuns.setText("0");
                                testView_comparisonResultDifferenceAvg.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getAverageExecutionTime() - performanceComparison.getWorkerBAnalyzation().getAverageExecutionTime()));
                                testView_comparisonResultDifferenceQ1.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getQuartil1ExecutionTime() - performanceComparison.getWorkerBAnalyzation().getQuartil1ExecutionTime()));
                                testView_comparisonResultDifferenceMean.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getMeanExecutionTime() - performanceComparison.getWorkerBAnalyzation().getMeanExecutionTime()));
                                testView_comparisonResultDifferenceQ3.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getQuartil3ExecutionTime() - performanceComparison.getWorkerBAnalyzation().getQuartil3ExecutionTime()));
                                testView_comparisonResultDifferenceMin.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getMinimumExecutionTime() - performanceComparison.getWorkerBAnalyzation().getMinimumExecutionTime()));
                                testView_comparisonResultDifferenceMax.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getMaximumExecutionTime() - performanceComparison.getWorkerBAnalyzation().getMaximumExecutionTime()));
                                testView_comparisonResultDifferenceSum.setText(Long.toString(performanceComparison.getWorkerAAnalyzation().getTotalExecutionTime() - performanceComparison.getWorkerBAnalyzation().getTotalExecutionTime()));
                                testView_comparisonResultRelativeRuns.setText("0");
                                testView_comparisonResultRelativeAvg.setText(String.format("%3.2f",(double) performanceComparison.getWorkerAAnalyzation().getAverageExecutionTime() / performanceComparison.getWorkerBAnalyzation().getAverageExecutionTime()));
                                testView_comparisonResultRelativeQ1.setText(String.format("%3.2f",(double) performanceComparison.getWorkerAAnalyzation().getQuartil1ExecutionTime() / performanceComparison.getWorkerBAnalyzation().getQuartil1ExecutionTime()));
                                testView_comparisonResultRelativeMean.setText(String.format("%3.2f",(double) performanceComparison.getWorkerAAnalyzation().getMeanExecutionTime() / performanceComparison.getWorkerBAnalyzation().getMeanExecutionTime()));
                                testView_comparisonResultRelativeQ3.setText(String.format("%3.2f",(double) performanceComparison.getWorkerAAnalyzation().getQuartil3ExecutionTime() / performanceComparison.getWorkerBAnalyzation().getQuartil3ExecutionTime()));
                                testView_comparisonResultRelativeMin.setText(String.format("%3.2f",(double) performanceComparison.getWorkerAAnalyzation().getMinimumExecutionTime() / performanceComparison.getWorkerBAnalyzation().getMinimumExecutionTime()));
                                testView_comparisonResultRelativeMax.setText(String.format("%3.2f",(double) performanceComparison.getWorkerAAnalyzation().getMaximumExecutionTime() / performanceComparison.getWorkerBAnalyzation().getMaximumExecutionTime()));
                                testView_comparisonResultRelativeSum.setText(String.format("%3.2f",(double) performanceComparison.getWorkerAAnalyzation().getTotalExecutionTime() / performanceComparison.getWorkerBAnalyzation().getTotalExecutionTime()));
                                progressDialog.dismiss();
                                button.setText("Run Test");
                                button.setEnabled(true);
                            }
                        });
                    }
                }, 0);
            }
        });
        return view;
    }

    private void initializeComparators(JavaWorker javaWorker, CppWorker cppWorker) {
        int defaultRuns = 10000;
        comparators.add(new PerformanceComparator("printHelloWorld()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.printHelloWorld();
            }
        });
        comparators.add(new PerformanceComparator("returnBoolean()", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnBoolean();
            }
        });
        comparators.add(new PerformanceComparator("returnByte()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnByte();
            }
        });
        comparators.add(new PerformanceComparator("returnShort()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnShort();
            }
        });
        comparators.add(new PerformanceComparator("returnInt()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnInt();
            }
        });
        comparators.add(new PerformanceComparator("returnLong()", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnLong();
            }
        });
        comparators.add(new PerformanceComparator("returnFloat()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnFloat();
            }
        });
        comparators.add(new PerformanceComparator("returnDouble()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnDouble();
            }
        });
        comparators.add(new PerformanceComparator("returnChar()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnChar();
            }
        });
        comparators.add(new PerformanceComparator("returnNewBooleanArray()", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnNewBooleanArray();
            }
        });
        comparators.add(new PerformanceComparator("returnBooleanArray(arr)", javaWorker, cppWorker, defaultRuns, true) {
            private boolean[] arr = new boolean[2];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnBooleanArray(arr);
            }
        });
        comparators.add(new PerformanceComparator("returnNewByteArray()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnNewByteArray();
            }
        });
        comparators.add(new PerformanceComparator("returnByteArray(arr)", javaWorker, cppWorker, defaultRuns, false) {
            private byte[] arr = new byte[2];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnByteArray(arr);
            }
        });
        comparators.add(new PerformanceComparator("returnNewShortArray()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnNewShortArray();
            }
        });
        comparators.add(new PerformanceComparator("returnShortArray(arr)", javaWorker, cppWorker, defaultRuns, false) {
            private short[] arr = new short[2];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnShortArray(arr);
            }
        });
        comparators.add(new PerformanceComparator("returnNewIntArray()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnNewIntArray();
            }
        });
        comparators.add(new PerformanceComparator("returnIntArray(arr)", javaWorker, cppWorker, defaultRuns, false) {
            private int[] arr = new int[2];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnIntArray(arr);
            }
        });
        comparators.add(new PerformanceComparator("returnNewLongArray()", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnNewLongArray();
            }
        });
        comparators.add(new PerformanceComparator("returnLongArray(arr)", javaWorker, cppWorker, defaultRuns, true) {
            private long[] arr = new long[2];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnLongArray(arr);
            }
        });
        comparators.add(new PerformanceComparator("returnNewFloatArray()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnNewFloatArray();
            }
        });
        comparators.add(new PerformanceComparator("returnFloatArray(arr)", javaWorker, cppWorker, defaultRuns, false) {
            private float[] arr = new float[2];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnFloatArray(arr);
            }
        });
        comparators.add(new PerformanceComparator("returnNewDoubleArray()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnNewDoubleArray();
            }
        });
        comparators.add(new PerformanceComparator("returnDoubleArray(arr)", javaWorker, cppWorker, defaultRuns, false) {
            private double[] arr = new double[2];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnDoubleArray(arr);
            }
        });
        comparators.add(new PerformanceComparator("returnNewCharArray()", javaWorker, cppWorker, defaultRuns, false) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnNewCharArray();
            }
        });
        comparators.add(new PerformanceComparator("returnCharArray(arr)", javaWorker, cppWorker, defaultRuns, false) {
            private char[] arr = new char[2];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.returnCharArray(arr);
            }
        });
        comparators.add(new PerformanceComparator("createSimpleObject(val)", javaWorker, cppWorker, defaultRuns * 2, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.createSimpleObject(4);
            }
        });
        comparators.add(new PerformanceComparator("setSimpleObjectValue(obj,val)", javaWorker, cppWorker, defaultRuns * 5, true) {
            private SimpleObject simpleObject = new SimpleObject(4);

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.setSimpleObjectValue(simpleObject, 4);
            }
        });
        comparators.add(new PerformanceComparator("getSimpleObjectValue(obj)", javaWorker, cppWorker, defaultRuns * 5, true) {
            private SimpleObject simpleObject = new SimpleObject(4);

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.getSimpleObjectValue(simpleObject);
            }
        });
        comparators.add(new PerformanceComparator("Fibonacci(100)", javaWorker, cppWorker, 70000, true) {
            private long[] arr = new long[100];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculateFibonacciNumbers(arr, 100);
            }
        });
        comparators.add(new PerformanceComparator("Fibonacci(5000)", javaWorker, cppWorker, 2200, true) {
            private long[] arr = new long[5000];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculateFibonacciNumbers(arr, 5000);
            }
        });
        comparators.add(new PerformanceComparator("Fibonacci(10000)", javaWorker, cppWorker, 1200, true) {
            private long[] arr = new long[10000];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculateFibonacciNumbers(arr, 10000);
            }
        });
        comparators.add(new PerformanceComparator("Fibonacci(100000)", javaWorker, cppWorker, 110, true) {
            private long[] arr = new long[100000];

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculateFibonacciNumbers(arr, 100000);
            }
        });
        comparators.add(new PerformanceComparator("markPrimes(50)", javaWorker, cppWorker, 90000, true) {
            private boolean[] arr = new boolean[50];

            @Override
            public void prepareWorkerMethodExecution(Worker worker) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i] = true;
                }
            }

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.markPrimes(arr, 50);
            }
        });
        comparators.add(new PerformanceComparator("markPrimes(75)", javaWorker, cppWorker, 70000, true) {
            private boolean[] arr = new boolean[75];

            @Override
            public void prepareWorkerMethodExecution(Worker worker) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i] = true;
                }
            }

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.markPrimes(arr, 75);
            }
        });
        comparators.add(new PerformanceComparator("markPrimes(100)", javaWorker, cppWorker, 60000, true) {
            private boolean[] arr = new boolean[100];

            @Override
            public void prepareWorkerMethodExecution(Worker worker) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i] = true;
                }
            }

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.markPrimes(arr, 100);
            }
        });
        comparators.add(new PerformanceComparator("markPrimes(1000)", javaWorker, cppWorker, 7000, true) {
            private boolean[] arr = new boolean[1000];

            @Override
            public void prepareWorkerMethodExecution(Worker worker) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i] = true;
                }
            }

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.markPrimes(arr, 1000);
            }
        });
        comparators.add(new PerformanceComparator("markPrimes(1000000)", javaWorker, cppWorker, 5, true) {
            private boolean[] arr = new boolean[1000000];

            @Override
            public void prepareWorkerMethodExecution(Worker worker) {
                for (int i = 0; i < arr.length; i++) {
                    arr[i] = true;
                }
            }

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.markPrimes(arr, 1000000);
            }
        });
        comparators.add(new PerformanceComparator("gps2ecefs(100)", javaWorker, cppWorker, 5, true) {
            private Coordinate[] arr = new Coordinate[100];
            private Coordinate[] gpsCoordinates = new Coordinate[100];

            {
                double lat = 47;
                double lon = 14;
                double alt = 300;
                for (int i = 0; i < gpsCoordinates.length; i++) {
                    gpsCoordinates[i] = new Coordinate(lat, lon, alt);
                    arr[i] = new Coordinate(0, 0, 0);
                    lat += 0.00005;
                    lon += 0.00005;
                    alt += 2;
                }
            }

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculateEcefCoordinatesFromGpsPositions(arr, gpsCoordinates);
            }
        });
        comparators.add(new PerformanceComparator("gps2ecefs(5000)", javaWorker, cppWorker, 5, true) {
            private Coordinate[] arr = new Coordinate[5000];
            private Coordinate[] gpsCoordinates = new Coordinate[5000];

            {
                double lat = 47;
                double lon = 14;
                double alt = 300;
                for (int i = 0; i < gpsCoordinates.length; i++) {
                    gpsCoordinates[i] = new Coordinate(lat, lon, alt);
                    arr[i] = new Coordinate(0, 0, 0);
                    lat += 0.00005;
                    lon += 0.00005;
                    alt += 2;
                }
            }

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculateEcefCoordinatesFromGpsPositions(arr, gpsCoordinates);
            }
        });
        comparators.add(new PerformanceComparator("gps2ecefs(500000)", javaWorker, cppWorker, 5, true) {
            private Coordinate[] arr = new Coordinate[500000];
            private Coordinate[] gpsCoordinates = new Coordinate[500000];

            {
                double lat = 47;
                double lon = 14;
                double alt = 300;
                for (int i = 0; i < gpsCoordinates.length; i++) {
                    gpsCoordinates[i] = new Coordinate(lat, lon, alt);
                    arr[i] = new Coordinate(0, 0, 0);
                    lat += 0.000005;
                    lon += 0.000005;
                    alt += 2;
                }
            }

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculateEcefCoordinatesFromGpsPositions(arr, gpsCoordinates);
            }
        });
        comparators.add(new PerformanceComparator("gps2ecef", javaWorker, cppWorker, defaultRuns, true) {
            private Coordinate coordinate = new Coordinate(0, 0, 0);

            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculateEcefCoordinateFromGpsPosition(coordinate, 47, 14, 300);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(1)", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(1);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(2)", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(2);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(3)", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(3);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(4)", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(3);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(5)", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(5);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(10)", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(10);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(25)", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(25);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(50)", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(50);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(75)", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(75);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(100)", javaWorker, cppWorker, defaultRuns, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(100);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(100000)", javaWorker, cppWorker, 500, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(100000);
            }
        });
        comparators.add(new PerformanceComparator("calculatePi(5000000)", javaWorker, cppWorker, 5, true) {
            @Override
            public void executeWorkerMethod(Worker worker) {
                worker.calculatePiByLeibniz(5000000);
            }
        });
    }
}
