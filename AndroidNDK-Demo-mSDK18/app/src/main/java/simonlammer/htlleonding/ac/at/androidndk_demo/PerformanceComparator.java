package simonlammer.htlleonding.ac.at.androidndk_demo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Simon Lammer on 09.12.2016.
 */

public abstract class PerformanceComparator {
    private String name;
    private Worker workerA;
    private Worker workerB;
    private int runs;
    private final int initialRuns;
    private boolean selected;

    public PerformanceComparator(String name, Worker workerA, Worker workerB, int runs, boolean selected) {
        this.name = name;
        this.workerA = workerA;
        this.workerB = workerB;
        this.initialRuns = runs;
        this.runs = runs;
        this.selected = selected;
    }

    public int getInitialRuns() {
        return initialRuns;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PerformanceComparison investigateTimeDifference() {
        return new PerformanceComparison(investigateTimeDifference(workerA), investigateTimeDifference(workerB));
    }

    private List<Long> investigateTimeDifference(Worker worker) {
        long timePreTest_ns;
        long timePostTest_ns;
        List<Long> executionTimes_ns = new ArrayList<>(runs);
        for (int i = 0; i < runs; i++) {
            prepareWorkerMethodExecution(worker);
            timePreTest_ns = System.nanoTime();
            executeWorkerMethod(worker);
            timePostTest_ns = System.nanoTime();
            executionTimes_ns.add(timePostTest_ns - timePreTest_ns);
        }
        return executionTimes_ns;
    }

    public void prepareWorkerMethodExecution(Worker worker) {
        // intentionally empty
    }

    public abstract void executeWorkerMethod(Worker worker);
}