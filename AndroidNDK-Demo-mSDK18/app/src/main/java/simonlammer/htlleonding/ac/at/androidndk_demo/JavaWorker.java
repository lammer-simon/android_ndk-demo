package simonlammer.htlleonding.ac.at.androidndk_demo;

/**
 * Created by Simon Lammer on 08.12.2016.
 */

public class JavaWorker implements Worker {
    @Override
    public void printHelloWorld() {
        System.out.println("Hello World");
    }

    @Override
    public boolean returnBoolean() {
        return true;
    }

    @Override
    public byte returnByte() {
        return 4;
    }

    @Override
    public short returnShort() {
        return 4;
    }

    @Override
    public int returnInt() {
        return 4;
    }

    @Override
    public long returnLong() {
        return 4;
    }

    @Override
    public float returnFloat() {
        return 4.4f;
    }

    @Override
    public double returnDouble() {
        return 4.4;
    }

    @Override
    public char returnChar() {
        return '4';
    }

    @Override
    public boolean[] returnNewBooleanArray() {
        return new boolean[] { true, false };
    }

    @Override
    public boolean[] returnBooleanArray(boolean[] arr) {
        arr[0] = true;
        arr[1] = false;
        return arr;
    }

    @Override
    public byte[] returnNewByteArray() {
        return new byte[] { 4, 4 };
    }

    @Override
    public byte[] returnByteArray(byte[] arr) {
        arr[0] = 4;
        arr[1] = 4;
        return arr;
    }

    @Override
    public short[] returnNewShortArray() {
        return new short[] {4, 4};
    }

    @Override
    public short[] returnShortArray(short[] arr) {
        arr[0] = 4;
        arr[1] = 4;
        return arr;
    }

    @Override
    public int[] returnNewIntArray() {
        return new int[] {4, 4};
    }

    @Override
    public int[] returnIntArray(int[] arr) {
        arr[0] = 4;
        arr[1] = 4;
        return arr;
    }

    @Override
    public long[] returnNewLongArray() {
        return new long[] {4, 4};
    }

    @Override
    public long[] returnLongArray(long[] arr) {
        arr[0] = 4;
        arr[1] = 4;
        return arr;
    }

    @Override
    public float[] returnNewFloatArray() {
        return new float[] {4.4f, 4.4f};
    }

    @Override
    public float[] returnFloatArray(float[] arr) {
        arr[0] = 4.4f;
        arr[1] = 4.4f;
        return arr;
    }

    @Override
    public double[] returnNewDoubleArray() {
        return new double[] {4.4, 4.4};
    }

    @Override
    public double[] returnDoubleArray(double[] arr) {
        arr[0] = 4.4;
        arr[1] = 4.4;
        return arr;
    }

    @Override
    public char[] returnNewCharArray() {
        return new char[] {'4', '4'};
    }

    @Override
    public char[] returnCharArray(char[] arr) {
        arr[0] = '4';
        arr[1] = '4';
        return arr;
    }

    @Override
    public SimpleObject createSimpleObject(int initialValue) {
        return new SimpleObject(initialValue);
    }
    @Override
    public void setSimpleObjectValue(SimpleObject simpleObject, int value) {
        simpleObject.setValue(value);
    }
    @Override
    public int getSimpleObjectValue(SimpleObject simpleObject) {
        return simpleObject.getValue();
    }

    @Override
    public long[] calculateFibonacciNumbers(long[] arr, int count) {
        arr[0] = 0;
        arr[1] = 1;
        for (int i = 2; i < count; i++) {
            arr[i] = arr[i - 1] + arr[i - 2];
        }
        return arr;
    }
    @Override
    public boolean[] markPrimes(boolean[] arr, int count) {
        int sqrt_count = (int) Math.ceil(Math.sqrt(count));
        arr[0] = false; // 0 is no prime number
        arr[1] = false; // 1 is no prime number
        for (int i = 2; i < sqrt_count; i++) {
            if (arr[i]) {
                for (int j = i * i; j < count; j += i) {
                    arr[j] = false;
                }
            }
        }
        return arr;
    }
    @Override
    public Coordinate[] calculateEcefCoordinatesFromGpsPositions(Coordinate[] arr, Coordinate[] gpsCoordinates) {
        // formulae: https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#From_geodetic_to_ECEF_coordinates
        double a = Constants.WGS84_A;
        double b = Constants.WGS84_B;
        double a_squared = a * a;
        double b_squared = b * b;
        double p, l, h, sin_p, sin_l, cos_p, cos_l, n, v, x, y, z;
        for (int i = 0; i < gpsCoordinates.length; i++) {
            p = (Math.PI / 180) * gpsCoordinates[i].getX();
            l = (Math.PI / 180) * gpsCoordinates[i].getY();
            h = gpsCoordinates[i].getZ();
            sin_p = Math.sin(p);
            sin_l = Math.sin(l);
            cos_p = Math.cos(p);
            cos_l = Math.cos(l);
            n = a_squared / Math.sqrt(a_squared * cos_p * cos_p + b_squared * sin_p * sin_p);
            v = n + h;
            x = v * cos_p * cos_l;
            y = v * cos_p * sin_l;
            z = (b_squared / a_squared * n + h) * sin_p ;
            arr[i].set(x, y, z);
        }
        return arr;
    }
    @Override
    public Coordinate calculateEcefCoordinateFromGpsPosition(Coordinate coordinate, double lat, double lon, double alt) {
        double a = Constants.WGS84_A;
        double b = Constants.WGS84_B;
        double a_squared = a * a;
        double b_squared = b * b;
        double p = (Math.PI / 180) * lat;
        double l = (Math.PI / 180) * lon;
        double h = alt;
        double sin_p = Math.sin(p);
        double sin_l = Math.sin(l);
        double cos_p = Math.cos(p);
        double cos_l = Math.cos(l);
        double n = a_squared / Math.sqrt(a_squared * cos_p * cos_p + b_squared * sin_p * sin_p);
        double v = n + h;
        double x = v * cos_p * cos_l;
        double y = v * cos_p * sin_l;
        double z = (b_squared / a_squared * n + h) * sin_p ;
        coordinate.set(x, y ,z);
        return coordinate;
    }
    @Override
    public double calculatePiByLeibniz(int accuracy) {
        double pi = 4;
        int denominator = 3;
        for (int i = 0; i < accuracy; i++) {
            pi -= 4.0 / denominator;
            denominator += 2;
            pi += 4.0 / denominator;
            denominator += 2;
        }
        return pi;
    }
}
