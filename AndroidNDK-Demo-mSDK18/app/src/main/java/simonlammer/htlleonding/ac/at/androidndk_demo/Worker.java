package simonlammer.htlleonding.ac.at.androidndk_demo;

/**
 * Created by Simon Lammer on 08.12.2016.
 */

public interface Worker {
    void printHelloWorld();

    boolean returnBoolean();
    byte returnByte();
    short returnShort();
    int returnInt();
    long returnLong();
    float returnFloat();
    double returnDouble();
    char returnChar();

    boolean[] returnNewBooleanArray();
    boolean[] returnBooleanArray(boolean[] arr);
    byte[] returnNewByteArray();
    byte[] returnByteArray(byte[] arr);
    short[] returnNewShortArray();
    short[] returnShortArray(short[] arr);
    int[] returnNewIntArray();
    int[] returnIntArray(int[] arr);
    long[] returnNewLongArray();
    long[] returnLongArray(long[] arr);
    float[] returnNewFloatArray();
    float[] returnFloatArray(float[] arr);
    double[] returnNewDoubleArray();
    double[] returnDoubleArray(double[] arr);
    char[] returnNewCharArray();
    char[] returnCharArray(char[] arr);

    SimpleObject createSimpleObject(int initialValue);
    void setSimpleObjectValue(SimpleObject simpleObject, int value);
    int getSimpleObjectValue(SimpleObject simpleObject);

    long[] calculateFibonacciNumbers(long[] arr, int count);
    boolean[] markPrimes(boolean[] arr, int count);
    Coordinate[] calculateEcefCoordinatesFromGpsPositions(Coordinate[] arr, Coordinate[] gpsCoordinates);
    Coordinate calculateEcefCoordinateFromGpsPosition(Coordinate coordinate, double lat, double lon, double alt);
    double calculatePiByLeibniz(int accuracy);
}
