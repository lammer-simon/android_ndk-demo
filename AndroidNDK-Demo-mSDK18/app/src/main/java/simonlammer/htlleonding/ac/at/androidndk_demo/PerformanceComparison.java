package simonlammer.htlleonding.ac.at.androidndk_demo;

import java.util.Collections;
import java.util.List;

/**
 * Created by Simon Lammer on 09.12.2016.
 */

public class PerformanceComparison {
    private WorkerAnalyzation workerAAnalyzation;
    private WorkerAnalyzation workerBAnalyzation;
    private WorkerAnalyzation workerAnalyzationDifference;
    private int runs;

    public PerformanceComparison(List<Long> workerAExecutionTimes_ns, List<Long> workerBExecutionTimes_ns) {
        workerAAnalyzation = analyzeWorkerExecutionTimes(workerAExecutionTimes_ns);
        workerBAnalyzation = analyzeWorkerExecutionTimes(workerBExecutionTimes_ns);
        workerAnalyzationDifference = new WorkerAnalyzation(null,
                workerAAnalyzation.getTotalExecutionTime() - workerBAnalyzation.getTotalExecutionTime(),
                workerAAnalyzation.getMinimumExecutionTime() - workerBAnalyzation.getMinimumExecutionTime(),
                workerAAnalyzation.getAverageExecutionTime() - workerBAnalyzation.getAverageExecutionTime(),
                workerAAnalyzation.getMeanExecutionTime() - workerBAnalyzation.getMeanExecutionTime(),
                workerAAnalyzation.getMaximumExecutionTime() - workerBAnalyzation.getMaximumExecutionTime(),
                workerAAnalyzation.getQuartil1ExecutionTime() - workerBAnalyzation.getQuartil1ExecutionTime(),
                workerAAnalyzation.getQuartil3ExecutionTime() - workerBAnalyzation.getQuartil3ExecutionTime()
        );
        runs = workerAExecutionTimes_ns.size();
    }

    private WorkerAnalyzation analyzeWorkerExecutionTimes(List<Long> executionTimes) {
        Collections.sort(executionTimes);
        long sum = 0;
        for (Long num : executionTimes) {
            sum += num;
        }
        return new WorkerAnalyzation(executionTimes,
                /* totalExecutionTime= */ sum,
                /* minimumExecutionTime= */ executionTimes.get(0),
                /* averageExecutionTime= */ sum / executionTimes.size(),
                /* meanExecutionTime= */ executionTimes.get(executionTimes.size()/2),
                /* maximumExecutionTime= */ executionTimes.get(executionTimes.size()-1),
                /* quartil1ExecutionTime= */ executionTimes.get(executionTimes.size()/4),
                /* quartil3ExecutionTime= */ executionTimes.get(executionTimes.size()*3/4)
        );
    }

    public int getRuns() {
        return runs;
    }

    public WorkerAnalyzation getWorkerAAnalyzation() {
        return workerAAnalyzation;
    }

    public WorkerAnalyzation getWorkerBAnalyzation() {
        return workerBAnalyzation;
    }

    public WorkerAnalyzation getWorkerAnalyzationDifference() {
        return workerAnalyzationDifference;
    }

    @Override
    public String toString() {
        return String.format(
                "Runs: %d\n" +
                "Avg;Mean;Min;Max;Sum;Q1;Q3\n" +
                "A: %d;%d;%d;%d;%d;%d;%d\n" +
                "B: %d;%d;%d;%d;%d;%d;%d\n",
                runs,
                workerAAnalyzation.getAverageExecutionTime(), workerAAnalyzation.getMeanExecutionTime(), workerAAnalyzation.getMinimumExecutionTime(), workerAAnalyzation.getMaximumExecutionTime(), workerAAnalyzation.getTotalExecutionTime(), workerAAnalyzation.getQuartil1ExecutionTime(), workerAAnalyzation.getQuartil3ExecutionTime(),
                workerBAnalyzation.getAverageExecutionTime(), workerBAnalyzation.getMeanExecutionTime(), workerBAnalyzation.getMinimumExecutionTime(), workerBAnalyzation.getMaximumExecutionTime(), workerBAnalyzation.getTotalExecutionTime(), workerBAnalyzation.getQuartil1ExecutionTime(), workerBAnalyzation.getQuartil3ExecutionTime()
        );
    }

    public class WorkerAnalyzation {
        private List<Long> executionTimes;
        private long totalExecutionTime;
        private long minimumExecutionTime;
        private long averageExecutionTime;
        private long meanExecutionTime;
        private long maximumExecutionTime;
        private long deltaExecutionTime; // max - min
        private long quartil1ExecutionTime;
        private long quartil3ExecutionTime;

        public WorkerAnalyzation(List<Long> executionTimes, long totalExecutionTime, long minimumExecutionTime, long averageExecutionTime, long meanExecutionTime, long maximumExecutionTime, long quartil1ExecutionTime, long quartil3ExecutionTime) {
            this.executionTimes = executionTimes;
            this.totalExecutionTime = totalExecutionTime;
            this.minimumExecutionTime = minimumExecutionTime;
            this.averageExecutionTime = averageExecutionTime;
            this.meanExecutionTime = meanExecutionTime;
            this.maximumExecutionTime = maximumExecutionTime;
            this.deltaExecutionTime = maximumExecutionTime - minimumExecutionTime;
            this.quartil1ExecutionTime = quartil1ExecutionTime;
            this.quartil3ExecutionTime = quartil3ExecutionTime;
        }

        public List<Long> getExecutionTimes() {
            return executionTimes;
        }

        public long getTotalExecutionTime() {
            return totalExecutionTime;
        }

        public long getMinimumExecutionTime() {
            return minimumExecutionTime;
        }

        public long getAverageExecutionTime() {
            return averageExecutionTime;
        }

        public long getMeanExecutionTime() {
            return meanExecutionTime;
        }

        public long getMaximumExecutionTime() {
            return maximumExecutionTime;
        }

        public long getDeltaExecutionTime() {
            return deltaExecutionTime;
        }

        public long getQuartil3ExecutionTime() {
            return quartil3ExecutionTime;
        }

        public long getQuartil1ExecutionTime() {
            return quartil1ExecutionTime;
        }
    }
}
