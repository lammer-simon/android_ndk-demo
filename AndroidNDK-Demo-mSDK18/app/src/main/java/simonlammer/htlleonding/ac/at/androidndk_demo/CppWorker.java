package simonlammer.htlleonding.ac.at.androidndk_demo;

/**
 * Created by Simon Lammer on 08.12.2016.
 */

public class CppWorker implements Worker {
    public static void registerNativeMethods() {
        if (!registerNativePerformanceTestMethods() ||
                !registerNativeObjectHandlingMethods()){
            throw new IllegalStateException("Could not register native methods!");
        }
    }
    private static native boolean registerNativePerformanceTestMethods();
    private static native boolean registerNativeObjectHandlingMethods();

    @Override
    public native void printHelloWorld();

    public native void printHelloWorld_WithoutParameters();

    @Override
    public native boolean returnBoolean();
    @Override
    public native byte returnByte();
    @Override
    public native short returnShort();
    @Override
    public native int returnInt();
    @Override
    public native long returnLong();
    @Override
    public native float returnFloat();
    @Override
    public native double returnDouble();
    @Override
    public native char returnChar();

    @Override
    public native boolean[] returnNewBooleanArray();
    @Override
    public native boolean[] returnBooleanArray(boolean[] arr);
    @Override
    public native byte[] returnNewByteArray();
    @Override
    public native byte[] returnByteArray(byte[] arr);
    @Override
    public native short[] returnNewShortArray();
    @Override
    public native short[] returnShortArray(short[] arr);
    @Override
    public native int[] returnNewIntArray();
    @Override
    public native int[] returnIntArray(int[] arr);
    @Override
    public native long[] returnNewLongArray();
    @Override
    public native long[] returnLongArray(long[] arr);
    @Override
    public native float[] returnNewFloatArray();
    @Override
    public native float[] returnFloatArray(float[] arr);
    @Override
    public native double[] returnNewDoubleArray();
    @Override
    public native double[] returnDoubleArray(double[] arr);
    @Override
    public native char[] returnNewCharArray();
    @Override
    public native char[] returnCharArray(char[] arr);

    @Override
    public native SimpleObject createSimpleObject(int initialValue);
    @Override
    public native void setSimpleObjectValue(SimpleObject simpleObject, int value);
    @Override
    public native int getSimpleObjectValue(SimpleObject simpleObject);

    @Override
    public native long[] calculateFibonacciNumbers(long[] arr, int count);
    @Override
    public native boolean[] markPrimes(boolean[] arr, int count);
    @Override
    public native Coordinate[] calculateEcefCoordinatesFromGpsPositions(Coordinate[] arr, Coordinate[] gpsCoordinates);
    @Override
    public native Coordinate calculateEcefCoordinateFromGpsPosition(Coordinate coordinate, double lat, double lon, double alt);
    @Override
    public native double calculatePiByLeibniz(int accuracy);
}
