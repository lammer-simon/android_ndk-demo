package simonlammer.htlleonding.ac.at.androidndk_demo;

/**
 * Created by Simon Lammer on 31.12.2016.
 */

public class Constants {
    public static final double WGS84_A = 6378137.0;
    public static final double WGS84_B = 6356752.3142;
}
