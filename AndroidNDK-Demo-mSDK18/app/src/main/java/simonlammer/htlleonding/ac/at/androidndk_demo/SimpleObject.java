package simonlammer.htlleonding.ac.at.androidndk_demo;

/**
 * Created by Simon Lammer on 29.12.2016.
 */

public class SimpleObject {
    private int value;

    public SimpleObject(int initialValue) {
        this.value = initialValue;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
