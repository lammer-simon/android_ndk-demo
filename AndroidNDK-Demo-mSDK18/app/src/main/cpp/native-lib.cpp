#include <jni.h>
#include <string>

extern "C"
jstring
Java_simonlammer_htlleonding_ac_at_androidndk_1demo_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
