#include <jni.h>
#include <iostream>

using namespace std;

extern "C" {
    JNIEXPORT void JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_printHelloWorld(JNIEnv *env,
                                                                                  jobject instance) {
        cout << "Hello World\n" << endl;
    }

    JNIEXPORT void JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_printHelloWorld_1WithoutParameters() {
        cout << "Hello World\n" << endl;
    }
}