#include <jni.h>
#include "native-globals.h"

namespace java {
    jclass simpleObject_class;
    jmethodID simpleObject_constructor_methodID;
    jmethodID simpleObject_setValue_methodID;
    jmethodID simpleObject_getValue_methodID;
    jclass coordinate_class;
    jmethodID coordinate_getX_methodID;
    jmethodID coordinate_getY_methodID;
    jmethodID coordinate_getZ_methodID;
    jmethodID coordinate_set_methodID;
}

namespace constants {
    double WGS84_A;
    double WGS84_B;
}

static bool loaded = false;

jclass findGlobalClass(JNIEnv* env, const char* name) {
    jclass local = env->FindClass(name);
    jclass global = (jclass) env->NewGlobalRef(local);
    env->DeleteLocalRef(local);
    return global;
}

extern "C"
void
Java_simonlammer_htlleonding_ac_at_androidndk_1demo_MainActivity_loadNativeLibrary(
        JNIEnv *env,
        jobject /* this */) {
    if (!loaded) {
        java::simpleObject_class = findGlobalClass(env, "simonlammer/htlleonding/ac/at/androidndk_demo/SimpleObject");
        java::simpleObject_constructor_methodID = env->GetMethodID(java::simpleObject_class, "<init>", "(I)V");
        java::simpleObject_setValue_methodID = env->GetMethodID(java::simpleObject_class, "setValue", "(I)V");
        java::simpleObject_getValue_methodID = env->GetMethodID(java::simpleObject_class, "getValue",  "()I");
        jclass constantsClass = env->FindClass("simonlammer/htlleonding/ac/at/androidndk_demo/Constants");
        jfieldID wgs84aFieldId = env->GetStaticFieldID(constantsClass, "WGS84_A", "D");
        jfieldID wgs84bFieldId = env->GetStaticFieldID(constantsClass, "WGS84_B", "D");
        constants::WGS84_A = env->GetStaticDoubleField(constantsClass, wgs84aFieldId);
        constants::WGS84_B = env->GetStaticDoubleField(constantsClass, wgs84bFieldId);
        java::coordinate_class = findGlobalClass(env, "simonlammer/htlleonding/ac/at/androidndk_demo/Coordinate");
        java::coordinate_getX_methodID = env->GetMethodID(java::coordinate_class, "getX", "()D");
        java::coordinate_getY_methodID = env->GetMethodID(java::coordinate_class, "getY", "()D");
        java::coordinate_getZ_methodID = env->GetMethodID(java::coordinate_class, "getZ", "()D");
        java::coordinate_set_methodID = env->GetMethodID(java::coordinate_class, "set", "(DDD)V");

        loaded = true;
    }
}

extern "C"
void
Java_simonlammer_htlleonding_ac_at_androidndk_1demo_MainActivity_unloadNativeLibrary(
        JNIEnv *env,
        jobject /* this */) {
    if (loaded) {
        env->DeleteGlobalRef(java::simpleObject_class);
        loaded = false;
    }
}