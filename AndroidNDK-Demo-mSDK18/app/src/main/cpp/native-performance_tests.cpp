#include <jni.h>
#include <string>
#include <math.h>
#include "native-globals.h"

jlongArray fibonacci (JNIEnv *env, jobject instance,
                      jlongArray arr, jint count) {
    jlong* elements = env->GetLongArrayElements(arr, NULL);
    elements[0] = 0;
    elements[1] = 1;
    for (int i = 2; i < count; ++i) {
        elements[i] = elements[i - 1] + elements[i - 2];
    }
    env->ReleaseLongArrayElements(arr, elements, NULL);
    return arr;
}

jbooleanArray mark_primes (JNIEnv *env, jobject instance,
                           jbooleanArray arr, jint count) {
    jboolean* elements = env->GetBooleanArrayElements(arr, NULL);
    int sqrt_count = (int) ceil(sqrt(count));
    elements[0] = false; // 0 is no prime number
    elements[1] = false; // 1 is no prime number
    for (int i = 2; i < sqrt_count; i++) {
        if (elements[i]) {
            for (int j = i * i; j < count; j += i) {
                elements[j] = false;
            }
        }
    }
    env->ReleaseBooleanArrayElements(arr, elements, NULL);
    return arr;
}

jobjectArray gps_to_ecefs (JNIEnv *env, jobject instance,
                           jobjectArray arr, jobjectArray gpsCoordinates) {
    // formulae: https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#From_geodetic_to_ECEF_coordinates
    double a = constants::WGS84_A;
    double b = constants::WGS84_B;
    double a_squared = a * a;
    double b_squared = b * b;
    double p, l, h, sin_p, sin_l, cos_p, cos_l, n, v, x, y, z;
    jsize gpsCoordinatesLength = env->GetArrayLength(gpsCoordinates);
    jobject gpsCoordinate, ecefCoordinate;
    for (int i = 0; i < gpsCoordinatesLength; i++) {
        gpsCoordinate = env->GetObjectArrayElement(gpsCoordinates, i);
        p = (M_PI / 180) * env->CallDoubleMethod(gpsCoordinate, java::coordinate_getX_methodID);
        l = (M_PI / 180) * env->CallDoubleMethod(gpsCoordinate, java::coordinate_getY_methodID);
        h = env->CallDoubleMethod(gpsCoordinate, java::coordinate_getZ_methodID);
        env->DeleteLocalRef(gpsCoordinate);
        sin_p = sin(p);
        sin_l = sin(l);
        cos_p = cos(p);
        cos_l = cos(l);
        n = a_squared / sqrt(a_squared * cos_p * cos_p + b_squared * sin_p * sin_p);
        v = n + h;
        x = v * cos_p * cos_l;
        y = v * cos_p * sin_l;
        z = (b_squared / a_squared * n + h) * sin_p;
        ecefCoordinate = env->GetObjectArrayElement(arr, i);
        env->CallVoidMethod(ecefCoordinate, java::coordinate_set_methodID, x, y, z);
        env->DeleteLocalRef(ecefCoordinate);
    }
    return arr;
}

jobject gps_to_ecef (JNIEnv *env, jobject instance,
                          jobject coordinate, jdouble lat, jdouble lon, jdouble alt) {
    // formulae: https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#From_geodetic_to_ECEF_coordinates
    double a = constants::WGS84_A;
    double b = constants::WGS84_B;
    double a_squared = a * a;
    double b_squared = b * b;
    double p = (M_PI / 180) * lat;
    double l = (M_PI / 180) * lon;
    double h = alt;
    double sin_p = sin(p);
    double sin_l = sin(l);
    double cos_p = cos(p);
    double cos_l = cos(l);
    double n = a_squared / sqrt(a_squared * cos_p * cos_p + b_squared * sin_p * sin_p);
    double v = n + h;
    double x = v * cos_p * cos_l;
    double y = v * cos_p * sin_l;
    double z = (b_squared / a_squared * n + h) * sin_p;
    env->CallVoidMethod(coordinate, java::coordinate_set_methodID, x, y, z);
    return coordinate;
}

jdouble calculatePiByLeibniz(JNIEnv* env, jobject instance, jint accuracy) {
    double pi = 4;
    int denominator = 3;
    for (int i = 0; i < accuracy; i++) {
        pi -= 4.0 / denominator;
        denominator += 2;
        pi += 4.0 / denominator;
        denominator += 2;
    }
    return pi;
}

/* ------------------------------------------------------------------
 * ---------------------- REGISTER METHODS --------------------------
 * ----------------------------------------------------------------*/

static JNINativeMethod methods[] = {
        {"calculateFibonacciNumbers", "([JI)[J", (void*) fibonacci},
        {"markPrimes", "([ZI)[Z", (void*) mark_primes},
        {"calculateEcefCoordinatesFromGpsPositions", "([Lsimonlammer/htlleonding/ac/at/androidndk_demo/Coordinate;[Lsimonlammer/htlleonding/ac/at/androidndk_demo/Coordinate;)[Lsimonlammer/htlleonding/ac/at/androidndk_demo/Coordinate;", (void*) gps_to_ecefs},
        {"calculateEcefCoordinateFromGpsPosition", "(Lsimonlammer/htlleonding/ac/at/androidndk_demo/Coordinate;DDD)Lsimonlammer/htlleonding/ac/at/androidndk_demo/Coordinate;", (void*) gps_to_ecef},
        {"calculatePiByLeibniz", "(I)D", (void*) calculatePiByLeibniz}
};

static const char *classPathName = "simonlammer/htlleonding/ac/at/androidndk_demo/CppWorker";

extern "C"
JNIEXPORT jboolean JNICALL
Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_registerNativePerformanceTestMethods(
        JNIEnv *env, jobject instance) {
    jclass clazz = env->FindClass(classPathName);
    if (!clazz){
        return false; // could not find class
    }
    return env->RegisterNatives(clazz, methods, sizeof(methods) / sizeof(methods[0])) >= 0;
}