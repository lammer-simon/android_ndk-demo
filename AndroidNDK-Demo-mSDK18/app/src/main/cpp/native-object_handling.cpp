#include <jni.h>
#include "native-globals.h"

jobject create_simple_object (JNIEnv *env,
                              jobject instance,
                              jint initialValue) {
    jobject simpleObject = env->NewObject(java::simpleObject_class, java::simpleObject_constructor_methodID, initialValue);
    return simpleObject;
}

void set_simple_object_value (JNIEnv *env,
                              jobject instance,
                              jobject simpleObject,
                              jint value) {
    env->CallVoidMethod(simpleObject, java::simpleObject_setValue_methodID, value);
}

jint get_simple_object_value (JNIEnv *env,
                              jobject instance,
                              jobject simpleObject) {
    jint simpleObjectValue = env->CallIntMethod(simpleObject, java::simpleObject_getValue_methodID);
    return simpleObjectValue;
}

/* ------------------------------------------------------------------
 * ---------------------- REGISTER METHODS --------------------------
 * ----------------------------------------------------------------*/

static JNINativeMethod methods[] = {
        {"createSimpleObject", "(I)Lsimonlammer/htlleonding/ac/at/androidndk_demo/SimpleObject;", (void*) create_simple_object},
        {"setSimpleObjectValue", "(Lsimonlammer/htlleonding/ac/at/androidndk_demo/SimpleObject;I)V", (void*) set_simple_object_value},
        {"getSimpleObjectValue", "(Lsimonlammer/htlleonding/ac/at/androidndk_demo/SimpleObject;)I", (void*) get_simple_object_value}
};

static const char *classPathName = "simonlammer/htlleonding/ac/at/androidndk_demo/CppWorker";

extern "C"
JNIEXPORT jboolean JNICALL
Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_registerNativeObjectHandlingMethods(
        JNIEnv *env, jobject instance) {
    jclass clazz = env->FindClass(classPathName);
    if (!clazz){
        return false; // could not find class
    }
    return env->RegisterNatives(clazz, methods, sizeof(methods) / sizeof(methods[0])) >= 0;
}