#include <jni.h>

extern "C" {
    JNIEXPORT jboolean JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnBoolean(JNIEnv *env,
                                                                                jobject instance) {
        jboolean result = true;
        return result;
    }

    JNIEXPORT jbyte JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnByte(JNIEnv *env,
                                                                             jobject instance) {
        jbyte result = 4;
        return result;
    }

    JNIEXPORT jshort JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnShort(JNIEnv *env,
                                                                              jobject instance) {
        jshort result = 4;
        return result;
    }

    JNIEXPORT jint JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnInt(JNIEnv *env,
                                                                            jobject instance) {
        jint result = 4;
        return result;
    }

    JNIEXPORT jlong JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnLong(JNIEnv *env,
                                                                             jobject instance) {
        jlong result = 4;
        return result;
    }

    JNIEXPORT jfloat JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnFloat(JNIEnv *env,
                                                                              jobject instance) {
        jfloat result = 4.4f;
        return result;
    }

    JNIEXPORT jdouble JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnDouble(JNIEnv *env,
                                                                               jobject instance) {
        jdouble result = 4.4;
        return result;
    }

    JNIEXPORT jchar JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnChar(JNIEnv *env,
                                                                             jobject instance) {
        jchar result = '4';
        return result;
    }
}