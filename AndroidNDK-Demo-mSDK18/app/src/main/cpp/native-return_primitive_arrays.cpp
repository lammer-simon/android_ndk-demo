#include <jni.h>
#include <string>

extern "C" {
    JNIEXPORT jbooleanArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnNewBooleanArray(JNIEnv *env,
                                                                                        jobject instance) {
        jbooleanArray arr = env->NewBooleanArray(2);
        jboolean* arr_elements = env->GetBooleanArrayElements(arr, false);
        arr_elements[0] = true;
        arr_elements[1] = false;
        env->ReleaseBooleanArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jbooleanArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnBooleanArray(JNIEnv *env,
                                                                                     jobject instance,
                                                                                     jbooleanArray arr) {
        jboolean* arr_elements = env->GetBooleanArrayElements(arr, false);
        arr_elements[0] = true;
        arr_elements[1] = false;
        env->ReleaseBooleanArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jbyteArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnNewByteArray(JNIEnv *env,
                                                                                     jobject instance) {
        jbyteArray arr = env->NewByteArray(2);
        jbyte* arr_elements = env->GetByteArrayElements(arr, false);
        arr_elements[0] = 4;
        arr_elements[1] = 4;
        env->ReleaseByteArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jbyteArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnByteArray(JNIEnv *env,
                                                                                  jobject instance,
                                                                                  jbyteArray arr) {
        jbyte* arr_elements = env->GetByteArrayElements(arr, false);
        arr_elements[0] = 4;
        arr_elements[1] = 4;
        env->ReleaseByteArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jshortArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnNewShortArray(JNIEnv *env,
                                                                                     jobject instance) {
        jshortArray arr = env->NewShortArray(2);
        jshort* arr_elements = env->GetShortArrayElements(arr, false);
        arr_elements[0] = 4;
        arr_elements[1] = 4;
        env->ReleaseShortArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jshortArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnShortArray(JNIEnv *env,
                                                                                  jobject instance,
                                                                                  jshortArray arr) {
        jshort* arr_elements = env->GetShortArrayElements(arr, false);
        arr_elements[0] = 4;
        arr_elements[1] = 4;
        env->ReleaseShortArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jintArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnNewIntArray(JNIEnv *env,
                                                                                      jobject instance) {
        jintArray arr = env->NewIntArray(2);
        jint* arr_elements = env->GetIntArrayElements(arr, false);
        arr_elements[0] = 4;
        arr_elements[1] = 4;
        env->ReleaseIntArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jintArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnIntArray(JNIEnv *env,
                                                                                   jobject instance,
                                                                                   jintArray arr) {
        jint* arr_elements = env->GetIntArrayElements(arr, false);
        arr_elements[0] = 4;
        arr_elements[1] = 4;
        env->ReleaseIntArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jlongArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnNewLongArray(JNIEnv *env,
                                                                                     jobject instance) {
        jlongArray arr = env->NewLongArray(2);
        jlong* arr_elements = env->GetLongArrayElements(arr, false);
        arr_elements[0] = 4;
        arr_elements[1] = 4;
        env->ReleaseLongArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jlongArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnLongArray(JNIEnv *env,
                                                                                  jobject instance,
                                                                                  jlongArray arr) {
        jlong* arr_elements = env->GetLongArrayElements(arr, false);
        arr_elements[0] = 4;
        arr_elements[1] = 4;
        env->ReleaseLongArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jfloatArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnNewFloatArray(JNIEnv *env,
                                                                                     jobject instance) {
        jfloatArray arr = env->NewFloatArray(2);
        jfloat* arr_elements = env->GetFloatArrayElements(arr, false);
        arr_elements[0] = 4.4f;
        arr_elements[1] = 4.4f;
        env->ReleaseFloatArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jfloatArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnFloatArray(JNIEnv *env,
                                                                                  jobject instance,
                                                                                  jfloatArray arr) {
        jfloat* arr_elements = env->GetFloatArrayElements(arr, false);
        arr_elements[0] = 4.4f;
        arr_elements[1] = 4.4f;
        env->ReleaseFloatArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jdoubleArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnNewDoubleArray(JNIEnv *env,
                                                                                      jobject instance) {
        jdoubleArray arr = env->NewDoubleArray(2);
        jdouble* arr_elements = env->GetDoubleArrayElements(arr, false);
        arr_elements[0] = 4.4;
        arr_elements[1] = 4.4;
        env->ReleaseDoubleArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jdoubleArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnDoubleArray(JNIEnv *env,
                                                                                   jobject instance,
                                                                                   jdoubleArray arr) {
        jdouble* arr_elements = env->GetDoubleArrayElements(arr, false);
        arr_elements[0] = 4.4;
        arr_elements[1] = 4.4;
        env->ReleaseDoubleArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jcharArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnNewCharArray(JNIEnv *env,
                                                                                       jobject instance) {
        jcharArray arr = env->NewCharArray(2);
        jchar* arr_elements = env->GetCharArrayElements(arr, false);
        arr_elements[0] = '4';
        arr_elements[1] = '4';
        env->ReleaseCharArrayElements(arr, arr_elements, NULL);
        return arr;
    }

    JNIEXPORT jcharArray JNICALL
    Java_simonlammer_htlleonding_ac_at_androidndk_1demo_CppWorker_returnCharArray(JNIEnv *env,
                                                                                    jobject instance,
                                                                                    jcharArray arr) {
        jchar* arr_elements = env->GetCharArrayElements(arr, false);
        arr_elements[0] = '4';
        arr_elements[1] = '4';
        env->ReleaseCharArrayElements(arr, arr_elements, NULL);
        return arr;
    }
}