//
// Created by Simon Lammer on 31.12.2016.
//

#ifndef ANDROIDNDK_DEMO_MSDK18_NATIVE_GLOBALS_H
#define ANDROIDNDK_DEMO_MSDK18_NATIVE_GLOBALS_H

namespace java {
    extern jclass simpleObject_class;
    extern jmethodID simpleObject_constructor_methodID;
    extern jmethodID simpleObject_setValue_methodID;
    extern jmethodID simpleObject_getValue_methodID;
    extern jclass coordinate_class;
    extern jmethodID coordinate_getX_methodID;
    extern jmethodID coordinate_getY_methodID;
    extern jmethodID coordinate_getZ_methodID;
    extern jmethodID coordinate_set_methodID;
}

namespace constants {
    extern double WGS84_A;
    extern double WGS84_B;
}
#endif //ANDROIDNDK_DEMO_MSDK18_NATIVE_GLOBALS_H
